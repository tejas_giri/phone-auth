from rest_framework import serializers
from django.contrib.auth import get_user_model

User = get_user_model()

class CreateUserSerializer(serializers.ModelSerializer):
	phone_number = serializers.CharField(min_length=10)

	class Meta:
		model = User
		fields = ('phone_number', 'password')

		def create(self, validated_data):
			user=User.objects.create(**validated_data)
			user.set_password(validated_data['password'])
			# user.save()
			return user
			

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('phone_number','id')
