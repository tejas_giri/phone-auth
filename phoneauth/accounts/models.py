from datetime import date
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager    
from django.contrib.auth.base_user import AbstractBaseUser
from django.core.validators import RegexValidator



#step2
class UserManager(BaseUserManager):
    def create_user(self, phone_number, password=None, is_staff=False, is_admin=False, is_active=True):
        """
        Creates and saves a User with the given phone_no and password.
        """
        if not phone_number:
            raise ValueError('User must have a phone number')
        if not password:
            raise ValueError('User must have a password')
        user_obj = self.model(phone_number=phone_number)
        user_obj.set_password(password)
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.active = is_active
        user_obj.save(using=self._db)
        return user_obj

    def create_staffuser(self, phone_number, password):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            phone_number,
            password=password,
        )
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, phone_number, password):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            phone_number,
            password=password,
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user

#step1
class User(AbstractBaseUser):
    phone_regex = RegexValidator( regex = r'^\+?1?\d{9,14}$',
    message="phone no must be entered in the format: +9999999999. Up to 14 digits allowed")
    phone_number = models.CharField(validators =[phone_regex], max_length=15, unique=True)
    active = models.BooleanField(default=True) #can login
    staff = models.BooleanField(default=False) #staff user non superuser
    admin = models.BooleanField(default=False) #superuser
    
    USERNAME_FIELD = 'phone_number' #sets phone_no as username
    #USERNAME_FIELD and password are required by default
    REQUIRED_FIELDS = [] #['full_name'] #python manage.py create superuser

    #step3
    objects = UserManager() # hook in the New Manager to our Model

    def __str__(self):
        return self.phone_number

    
    def get_full_name(self): #default method
        return self.phone_number

    def get_short_name(self): #default method
        return self.phone_number

    def has_perm(self, perm, obj=None): #added later 
        return True

    def has_module_perms(self, app_label): #added later 
        return True

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_active(self):
        return self.active


class PhoneOTP(models.Model):
    phone_regex = RegexValidator( regex = r'^\+?1?\d{9,14}$',
    message="phone no must be entered in the format: +9999999999. Up to 14 digits allowed")
    phone_number = models.CharField(validators =[phone_regex], max_length=15, unique=True)
    otp = models.CharField(max_length=9, blank=True, null=True)
    count = models.IntegerField(default=0, help_text='no of otp sent')
    validated = models.BooleanField(default=False, help_text='if true user have validated otp correctly')
    
    def __str__(self):
        return str(self.phone_number) + ' is sent ' + str(self.otp)

    
    