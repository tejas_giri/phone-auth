
#     def get_user(user_id):
#         try:
#             return User.objects.get(pk=user_id)
#         except User.DoesNotExist:
#             return None


from rest_framework.authentication import BaseAuthentication
from rest_framework import exceptions

from django.contrib.auth import get_user_model

User = get_user_model()

class PasswordlessAuthBackend(BaseAuthentication):
    def authenticate(phone_number=None):
        phone_number = phone_number
        if not phone_number: # no username passed in request headers
            return None # authentication did not succeed

        try:
            user = User.objects.get(phone_number=phone_number) # get the user
            return user
        except User.DoesNotExist:
            return None # raise exception if user does not exist

class PasswordAuthBackend(BaseAuthentication):
    def authenticate(phone_number=None,password=None):
        try:
            user = User.objects.get(phone_number=phone_number)
        except User.DoesNotExist:
            return None
        else:
            if user.password == (password):
                return user
        return None
