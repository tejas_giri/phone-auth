from django.urls import path, include, re_path
from .views import (RegisterPhoneVerification, RegisterOtpVerification, Register,
					LoginPhoneVerification,LoginOtpVerification, LoginVerification
) 


app_name='accounts'

urlpatterns = [
   	re_path(r'^register/validate/phone/', RegisterPhoneVerification.as_view()),
   	re_path(r'^register/validate/otp/', RegisterOtpVerification.as_view()),
   	re_path(r'^register/', Register.as_view()),

   	re_path(r'^login/validate/phone/', LoginPhoneVerification.as_view()),
   	re_path(r'^login/validate/otp/', LoginOtpVerification.as_view()),   	
   	
   	path('login/', LoginVerification.as_view()),


]