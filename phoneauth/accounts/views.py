from rest_framework import authentication, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny

from rest_framework import status


from django.contrib.auth import authenticate


from .backends import PasswordAuthBackend as pass_auth
from .utils import (send_otp, create_otp, verify_otp,login_with_otp)
from .models import User, PhoneOTP 
from .serializers import CreateUserSerializer,UserSerializer




class RegisterPhoneVerification(APIView):
	authentication_classes = [authentication.TokenAuthentication]
	permission_classes = [permissions.AllowAny]

	def post(self, request, *args, **kwargs):
		phone = request.data.get('phone_number')

		if phone and len(phone) > 9:
			phone_number = str(phone)
			user = User.objects.filter(phone_number__iexact=phone_number)
			if user.exists():
				
				return Response(
					{'error':'account already exists.'},
					status=status.HTTP_409_CONFLICT
				)
				
			else:
				key = create_otp(phone_number)
				if key:
					response = send_otp(phone_number, key)
					return response
				else:
					return Response(
						{'error':'sending otp error, please try again.'},
						status=status.HTTP_500_INTERNAL_SERVER_ERROR
					)

		else: 
			return Response(
				{'error':'mobile number not valid, please provide valid mobile number.'},
				status=status.HTTP_400_BAD_REQUEST
			)



class RegisterOtpVerification(APIView):
	'''if u have received otp, post a request with phone no and otp,
	 you will be redirected to set password'''

	authentication_classes = [authentication.TokenAuthentication]
	permission_classes = [permissions.AllowAny]

	def post(self, request, *args, **kwargs):
		phone_number = request.data.get('phone_number', False)
		otp_sent = request.data.get('otp', False)

		if phone_number and otp_sent:
			response = verify_otp(phone_number, otp_sent)
			return response
		else:
			return Response(
				{'error':'please provide both  mobile number and otp for validation.'},
				status=status.HTTP_409_CONFLICT
			)





class Register(APIView):

	authentication_classes = [authentication.TokenAuthentication]
	permission_classes = [permissions.AllowAny]

	def post(self, request, *args, **kwargs):
		phone_number = request.data.get('phone_number', False)
		password = request.data.get('password', False)

		if phone_number and password:
			old= PhoneOTP.objects.filter(phone_number__iexact=phone_number)
			if old.exists():
				old = old.first()
				validated = old.validated
				
				if validated:
					temp_data = {
						'phone_number': phone_number,
						'password': password
					}

					serializer =  CreateUserSerializer(data= temp_data)
					serializer.is_valid(raise_exception = True)
					serializer.save();
					old.delete()
					return Response(
						{'data':'account created successfully.'},
						status=status.HTTP_201_CREATED
					)	
					
				else:
					return Response(
						{'error':'invalid otp, please try again.'},
						status=status.HTTP_400_BAD_REQUEST
					)
			else:
				
				return Response(
					{'error':'Unable to verify mobile number +91'+phone_number+'. please resend otp.'},
					status=status.HTTP_409_CONFLICT
				)

			
		else:
			return Response(
				{'error':'please provide mobile number and password.'},
				status=status.HTTP_409_CONFLICT
			)
			



class LoginPhoneVerification(APIView):

	authentication_classes = [authentication.TokenAuthentication]
	permission_classes = [permissions.AllowAny]

	def post(self, request, *args, **kwargs):
		phone = request.data.get('phone_number')
		if phone and len(phone) > 9:
			phone_number = str(phone)
			user = User.objects.filter(phone_number__iexact=phone_number)
			if user.exists():
				key = create_otp(phone_number)
				if key:
					response = send_otp(phone_number, key)
					return response
				else:
					return Response(
						{'error':'sending otp error, please try again.'},
						status=status.HTTP_500_INTERNAL_SERVER_ERROR
					)
			else:
				return Response(
					{'error':'account does not exists. please resgiter first. '},
					status=status.HTTP_409_CONFLICT
				)
		else: 
			return Response(
				{'error':'mobile number is not valid, please provide valid mobile number.'},
				status=status.HTTP_400_BAD_REQUEST
			)


class LoginOtpVerification(APIView):
	authentication_classes = [authentication.TokenAuthentication]
	permission_classes = [permissions.AllowAny]
	def post(self, request, *args, **kwargs):
		phone_number = request.data.get('phone_number', False)
		otp_sent = request.data.get('otp', False)
		if phone_number and otp_sent:
			response = verify_otp(phone_number, otp_sent)
			if not 'error' in response.data:
				response = login_with_otp(phone_number)
				return response
			else:
				return response	

		else:
			return Response(
				{'error':'please provide both  mobile number and otp for validation.'},
				status=status.HTTP_409_CONFLICT
			)




   
class LoginVerification(APIView):
	authentication_classes = [authentication.TokenAuthentication]
	permission_classes = [permissions.AllowAny]
	def post(self, request, *args, **kwargs):
		phone_number = request.data.get('phone_number', False)
		password = request.data.get('password', False)
		user = pass_auth.authenticate(phone_number=phone_number, password=password)
		if user:
			serializer = UserSerializer(user)
			token, _ = Token.objects.get_or_create(user=user)

			return Response(
				{
					'data':{
						'token':token.key,
						'user':serializer.data
					}
				},	
				status=status.HTTP_202_ACCEPTED
			)	

		else:
	 		return Response(
				{'error':'invalid mobile number or password, please try again.'},
				status=status.HTTP_400_BAD_REQUEST
			)
	 	







