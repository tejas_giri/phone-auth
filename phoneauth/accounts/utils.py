from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import User, PhoneOTP 
from django.shortcuts import get_object_or_404
from django.contrib import auth
from django.contrib.auth import authenticate, login
import random
 
from django.conf import settings

from rest_framework.authtoken.models import Token


from accounts.backends import PasswordlessAuthBackend as otp_auth

from accounts.serializers import UserSerializer
    # TODO: Define serializer fields here
    

from rest_framework import status

def create_otp(phone_number):
	if phone_number:
		key = random.randint(999, 9999)
		print(key)
		return key
	else:
		return Response(
			{'error':'unable to send otp, please try again.'},
			status=status.HTTP_400_BAD_REQUEST
		)


def send_otp(phone_number,key):
	old = PhoneOTP.objects.filter(phone_number__iexact=phone_number)
	if old.exists():
		old = old.first()
		count = old.count
		if count > 10:
			return Response(
				{'error':'sending otp error. limit exceeded.'},
				status=status.HTTP_429_TOO_MANY_REQUESTS
			)

		old.count = count + 1 
		old.save()
		
		return Response(
			{'data':'otp sent successfully.'},
			status=status.HTTP_201_CREATED
		)

	else:
		
		# link = send_sms(phone_number, key)
		PhoneOTP.objects.create(
			phone_number = phone_number,
			otp = key
		)

		return Response(
			{'data':'otp sent successfully.'},
			status=status.HTTP_201_CREATED
		)	


def verify_otp(phone_number, otp_sent):
	old = PhoneOTP.objects.filter(phone_number__iexact=phone_number)
	if old.exists():
		old = old.first()
		otp = old.otp
		if str(otp_sent) == str(otp):
			old.validated=True
			old.save()
			return Response(
				{'data':'your mobile number is verified successfully.'},
				status=status.HTTP_202_ACCEPTED
			)
			
		else:
			return Response(
				{'error':'invalid otp, please try again.'},
				status=status.HTTP_400_BAD_REQUEST
			)

	else:

		return Response(
			{'error':'Unable to verify mobile number +91-'+phone_number+'. please resend otp.'},
			status=status.HTTP_409_CONFLICT
		)

			




def send_sms(phone_number, otp):
	
		
	import http.client
	conn = http.client.HTTPConnection("2factor.in")

	key = settings.SMS_KEY
	phone_number = "+91"+phone_number
	otp = str(otp)	
	payload = ""

	headers = { 'content-type': "application/x-www-form-urlencoded" }
	
	conn.request("GET", '/API/V1/'+key+'/SMS/'+phone_number+'/'+otp+'/KSS@2018', payload, headers)

	res = conn.getresponse()
	data = res.read()

	print(data.decode("utf-8"))

	return data



def login_with_otp(phone_number):
	old=PhoneOTP.objects.filter(phone_number__iexact=phone_number)
	if old.exists():
		old = old.first()
		validated = old.validated
		if validated:
			user = otp_auth.authenticate(phone_number=phone_number)
			if user:
				serializer = UserSerializer(user)
				token, _ = Token.objects.get_or_create(user=user)
				old.delete()

				return Response(
					{
						'data':{
							'token':token.key,
							'user':serializer.data
						}
					},	
					status=status.HTTP_202_ACCEPTED
				)	

			else:
				return Response(
					{'error':'invalid mobile number or otp, please try again.'},
					status=status.HTTP_400_BAD_REQUEST
				)
		 	
		else:
			return Response(
				{'error':'invalid otp, please try again.'},
				status=status.HTTP_400_BAD_REQUEST
			)

	else:
		return Response(
			{'error':'Unable to verify mobile number +91-'+phone_number+'. please resend otp.'},
			status=status.HTTP_409_CONFLICT
		)
