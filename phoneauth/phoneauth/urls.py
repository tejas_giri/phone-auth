from django.urls import path, include,re_path
# from django.conf.urls import  url
from django.contrib import admin
from django.conf import settings

urlpatterns = [
#    re_path(r'^/', admin.site.urls,name="admin"),
   re_path(r'^admin/', admin.site.urls),
   re_path(r'^api/', include('accounts.urls')),
]